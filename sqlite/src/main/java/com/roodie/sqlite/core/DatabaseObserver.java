package com.roodie.sqlite.core;

import android.database.ContentObserver;

/**
 * @author Roodie
 */
class DatabaseObserver extends ContentObserver {

    public DatabaseObserver() {
        super(MainHandler.getHandler());
    }

}
