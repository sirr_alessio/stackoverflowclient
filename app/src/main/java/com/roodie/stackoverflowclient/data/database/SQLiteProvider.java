package com.roodie.stackoverflowclient.data.database;

import android.support.annotation.NonNull;

import com.roodie.sqlite.core.SQLiteConfig;
import com.roodie.sqlite.core.SQLiteContentProvider;
import com.roodie.sqlite.core.SQLiteSchema;


/**
 * @author Roodie
 */
public class SQLiteProvider extends SQLiteContentProvider {

    private static final String DATABASE_NAME = "com.roodie.stackoverflowclient.database";
    private static final String CONTENT_AUTHORITY = "com.roodie.stackoverflowclient";

    @Override
    protected void prepareConfig(@NonNull SQLiteConfig config) {
        config.setDatabaseName(DATABASE_NAME);
        config.setAuthority(CONTENT_AUTHORITY);
    }

    @Override
    protected void prepareSchema(@NonNull SQLiteSchema schema) {
        schema.register(UserTable.TABLE);
        schema.register(QuestionTable.TABLE);
        schema.register(AnswerTable.TABLE);
        schema.register(TagTable.TABLE);
        schema.register(NotificationTable.TABLE);
    }
}
