package com.roodie.stackoverflowclient.data.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.roodie.sqlite.core.BaseTable;
import com.roodie.sqlite.core.Table;
import com.roodie.sqlite.utils.TableBuilder;
import com.roodie.stackoverflowclient.model.content.Notification;

import org.sqlite.database.sqlite.SQLiteDatabase;


/**
 * @author Roodie
 */
public class NotificationTable extends BaseTable<Notification> {

    public static final Table<Notification> TABLE = new NotificationTable();

    public static final String IS_UNREAD = "is_unread";
    public static final String CREATION_DATE = "creation_date";
    public static final String BODY = "body";

    @Override
    public void onCreate(@NonNull SQLiteDatabase database) {
        TableBuilder.create(this)
                .intColumn(IS_UNREAD)
                .textColumn(CREATION_DATE)
                .textColumn(BODY)
                .execute(database);
    }

    @NonNull
    @Override
    public ContentValues toValues(@NonNull Notification notification) {
        ContentValues values = new ContentValues();
        values.put(IS_UNREAD, notification.isUnread() ? 1 : 0);
        values.put(CREATION_DATE, String.valueOf(notification.getCreationDate()));
        values.put(BODY, notification.getBody());
        return values;
    }

    @NonNull
    @Override
    public Notification fromCursor(@NonNull Cursor cursor) {
        Notification notification = new Notification();
        notification.setUnread(cursor.getInt(cursor.getColumnIndex(IS_UNREAD)) > 0);
        notification.setCreationDate(Long.parseLong(cursor.getString(cursor.getColumnIndex(CREATION_DATE))));
        notification.setBody(cursor.getString(cursor.getColumnIndex(BODY)));
        return notification;
    }
}
