package com.roodie.stackoverflowclient.data.database;

import android.support.annotation.NonNull;

import com.roodie.sqlite.core.SQLite;
import com.roodie.sqlite.core.Where;
import com.roodie.sqlite.rx.RxSQLite;
import com.roodie.stackoverflowclient.api.RepositoryProvider;
import com.roodie.stackoverflowclient.app.analytics.Analytics;
import com.roodie.stackoverflowclient.app.analytics.EventKeys;
import com.roodie.stackoverflowclient.app.analytics.EventTags;
import com.roodie.stackoverflowclient.model.content.Question;
import com.roodie.stackoverflowclient.model.content.Tag;
import com.roodie.stackoverflowclient.model.content.User;
import com.roodie.stackoverflowclient.rx.RxSchedulers;
import com.roodie.stackoverflowclient.rx.StubAction;

import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * @author Roodie
 */
public class LocalRepository {

    @NonNull
    public Observable<User> getCurrentUser() {
        return RepositoryProvider.provideKeyValueStorage().getCurrentUserId()
                .take(1)
                .filter(id -> id > 0)
                .map(String::valueOf)
                .flatMap(id -> RxSQLite.get().querySingle(UserTable.TABLE, Where.create().equalTo(UserTable.USER_ID, id)))
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<Question>> questions(@NonNull String tag) {
        return RxSQLite.get().query(QuestionTable.TABLE, Where.create().equalTo(QuestionTable.TAG, tag))
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<String>> tags() {
        return RxSQLite.get().query(TagTable.TABLE, Where.create()).compose(RxSchedulers.async());
    }

    public boolean updateTag(@NonNull Tag tag) {
        if (tag.isFavourite()) {
            SQLite.get().delete(TagTable.TABLE, Where.create().equalTo(TagTable.TAG, tag.getName()));
            Analytics.buildEvent()
                    .putString(EventKeys.TAG, tag.getName())
                    .log(EventTags.TAGS_REMOVE_FAVOURITE);
            return false;
        } else {
            SQLite.get().insert(TagTable.TABLE, tag.getName());
            Analytics.buildEvent()
                    .putString(EventKeys.TAG, tag.getName())
                    .log(EventTags.TAGS_ADD_FAVOURITE);
            return true;
        }
    }

    public void logout() {
        Observable.just(true)
                .flatMap(value -> {
                    SQLite.get().delete(UserTable.TABLE);
                    SQLite.get().delete(QuestionTable.TABLE);
                    SQLite.get().delete(TagTable.TABLE);
                    SQLite.get().delete(AnswerTable.TABLE);
                    return Observable.just(value);
                })
                .subscribeOn(Schedulers.io())
                .subscribe(new StubAction<>(), new StubAction<>());
    }
}