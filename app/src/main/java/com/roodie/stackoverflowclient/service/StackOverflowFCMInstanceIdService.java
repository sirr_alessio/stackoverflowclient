package com.roodie.stackoverflowclient.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.roodie.stackoverflowclient.app.analytics.Analytics;
import com.roodie.stackoverflowclient.utils.TextUtils;


/**
 * @author Roodie
 */
public class StackOverflowFCMInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh()  {
        String token = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(token)) {
            Analytics.setFcmRegistrationKey(token);
        }
    }

}
