package com.roodie.stackoverflowclient.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.roodie.stackoverflowclient.R;
import com.roodie.stackoverflowclient.adapter.QuestionAdapter;
import com.roodie.stackoverflowclient.app.analytics.Analytics;
import com.roodie.stackoverflowclient.app.analytics.EventKeys;
import com.roodie.stackoverflowclient.app.analytics.EventTags;
import com.roodie.stackoverflowclient.dialog.LoadingDialog;
import com.roodie.stackoverflowclient.model.content.Answer;
import com.roodie.stackoverflowclient.model.content.Question;
import com.roodie.stackoverflowclient.presenter.QuestionPresenter;
import com.roodie.stackoverflowclient.rx.RxError;
import com.roodie.stackoverflowclient.utils.Views;
import com.roodie.stackoverflowclient.view.QuestionView;

import java.util.List;


/**
 * @author Roodie
 */
public class QuestionActivity extends AppCompatActivity implements QuestionView {

    private static final String QUESTION_KEY = "question";

    private QuestionAdapter mAdapter;

    private QuestionPresenter mPresenter;

    public static void start(@NonNull Activity activity, @NonNull Question question) {
        Intent intent = new Intent(activity, QuestionActivity.class);
        intent.putExtra(QUESTION_KEY, question);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_question);
        setSupportActionBar(Views.findById(this, R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.app_name);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        RecyclerView recyclerView = Views.findById(this, R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new QuestionAdapter();
        recyclerView.setAdapter(mAdapter);

        Question question = (Question) getIntent().getSerializableExtra(QUESTION_KEY);
        Analytics.buildEvent()
                .putString(EventKeys.QUESTION_ID, String.valueOf(question.getQuestionId()))
                .log(EventTags.SCREEN_QUESTION);
        mPresenter = new QuestionPresenter(this, getLoaderManager(), this,
                LoadingDialog.view(getSupportFragmentManager()),
                RxError.view(this, getSupportFragmentManager()),
                question);
        mPresenter.init(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPresenter.onSaveInstanceState(outState);
    }

    @Override
    public void showQuestion(@NonNull Question question) {
        mAdapter.setQuestion(question);
    }

    @Override
    public void showAnswers(@NonNull List<Answer> answers) {
        mAdapter.changeDataSet(answers);
    }
}
