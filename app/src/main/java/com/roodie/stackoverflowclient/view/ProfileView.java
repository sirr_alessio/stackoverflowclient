package com.roodie.stackoverflowclient.view;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.content.Badge;
import com.roodie.stackoverflowclient.model.content.UserTag;

import java.util.List;


/**
 * @author Roodie
 */
public interface ProfileView {

    void showUserName(@NonNull String name);

    void showUserImage(@NonNull String url);

    void showProfileLink(@NonNull String profileLink);

    void showReputation(@NonNull String reputation);

    void showBadges(@NonNull List<Badge> badges);

    void showTopTags(@NonNull List<UserTag> tags);

}
