package com.roodie.stackoverflowclient.view;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.content.Answer;
import com.roodie.stackoverflowclient.model.content.Question;

import java.util.List;


/**
 * @author Roodie
 */
public interface QuestionView {

    void showQuestion(@NonNull Question question);

    void showAnswers(@NonNull List<Answer> answers);

}
