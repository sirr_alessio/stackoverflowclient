package com.roodie.stackoverflowclient.view;

import android.support.annotation.NonNull;

/**
 * @author Roodie
 */
public interface ErrorView {

    void showNetworkError();

    void showUnexpectedError();

    void showErrorMessage(@NonNull String message);

}
