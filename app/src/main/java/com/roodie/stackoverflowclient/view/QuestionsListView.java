package com.roodie.stackoverflowclient.view;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.content.Question;

import java.util.List;


/**
 * @author Roodie
 */
public interface QuestionsListView extends EmptyListView {

    void showQuestions(@NonNull List<Question> questions);

    void addQuestions(@NonNull List<Question> questions);

    void hideRefresh();
}
