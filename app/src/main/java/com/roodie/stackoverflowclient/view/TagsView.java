package com.roodie.stackoverflowclient.view;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.roodie.stackoverflowclient.model.content.Tag;

import java.util.List;


/**
 * @author Roodie
 */
public interface TagsView extends EmptyListView {

    void clearText();

    void setEmptyText(@StringRes int textId);

    void hideAllElements();

    void showTags(@NonNull List<Tag> tags);

    void showFirstVisibleItem(int position);

    void showClearButton();

    void hideClearButton();

    void notifyChanged();
}
