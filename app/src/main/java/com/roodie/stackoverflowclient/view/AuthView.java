package com.roodie.stackoverflowclient.view;

import android.support.annotation.NonNull;

/**
 * @author Roodie
 */
public interface AuthView {

    void showAuth(@NonNull String url);

    void showWalkthrough();

    void showMainScreen();

    void close();

}
