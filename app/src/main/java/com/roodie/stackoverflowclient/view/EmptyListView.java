package com.roodie.stackoverflowclient.view;

/**
 * @author Roodie
 */
public interface EmptyListView {

    void showEmptyListView();

    void hideEmptyListView();
}
