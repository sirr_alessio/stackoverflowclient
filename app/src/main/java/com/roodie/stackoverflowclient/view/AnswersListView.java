package com.roodie.stackoverflowclient.view;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.roodie.stackoverflowclient.model.content.Answer;

import java.util.List;


/**
 * @author Roodie
 */
public interface AnswersListView extends EmptyListView {

    void showAnswers(@NonNull List<Answer> answers);

    void setEmptyText(@StringRes int textResId);

    void browseUrl(@NonNull String url);

}
