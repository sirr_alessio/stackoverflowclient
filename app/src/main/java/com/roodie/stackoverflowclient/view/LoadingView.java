package com.roodie.stackoverflowclient.view;

/**
 * @author Roodie
 */
public interface LoadingView {

    void showLoadingIndicator();

    void hideLoadingIndicator();

}
