package com.roodie.stackoverflowclient.view;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.content.User;

import java.util.List;


/**
 * @author Roodie
 */
public interface MainView {

    void showUserImage(@NonNull String imageUrl);

    void showUserName(@NonNull String name);

    void clearTabs();

    void addTab(@NonNull String tabTitle);

    void showTags(@NonNull List<String> tags);

    void openProfile(@NonNull User currentUser);

    void openAnswers(@NonNull User currentUser);

    void hideTabLayout();
}
