package com.roodie.stackoverflowclient.view;

import android.support.annotation.StringRes;

/**
 * @author Roodie
 */
public interface WalkthroughView {

    void showBenefit(int index);

    void setActionButtonText(@StringRes int textResId);

    void showLoadingSplash();

    void showError();

    void finishWalkthrough();

}
