package com.roodie.stackoverflowclient.utils;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.view.View;

/**
 * Created by Roodie on 10.10.2016.
 */

public final class Views {

    private Views() {
    }

    @NonNull
    public static <T extends View> T findById(@NonNull Activity activity, @IdRes int viewId) {
        //noinspection unchecked
        return (T) activity.findViewById(viewId);
    }

    @NonNull
    public static <T extends View> T findById(@NonNull View view, @IdRes int viewId) {
        //noinspection unchecked
        return (T) view.findViewById(viewId);
    }
}
