package com.roodie.stackoverflowclient.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.roodie.stackoverflowclient.R;
import com.roodie.stackoverflowclient.model.content.Answer;
import com.roodie.stackoverflowclient.utils.HtmlCompat;
import com.roodie.stackoverflowclient.utils.PicassoUtils;
import com.roodie.stackoverflowclient.utils.Views;
import com.squareup.picasso.Picasso;


/**
 * @author Roodie
 */
public class AnswerViewHolder extends RecyclerView.ViewHolder {

    private final ImageView mAuthorIcon;
    private final TextView mAuthorName;
    private final TextView mAnswerBody;
    private final View mAnsweredIcon;
    private final View mDivider;

    public AnswerViewHolder(View itemView) {
        super(itemView);

        mAuthorIcon = Views.findById(itemView, R.id.icon);
        mAuthorName = Views.findById(itemView, R.id.authorName);
        mAnswerBody = Views.findById(itemView, R.id.answerBody);
        mAnsweredIcon = Views.findById(itemView, R.id.answeredView);
        mDivider = Views.findById(itemView, R.id.divider);

        mAnswerBody.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void bind(@NonNull Answer answer, boolean isLast, int maxBodyLength) {
        Picasso.with(mAuthorIcon.getContext())
                .load(answer.getOwner().getProfileImage())
                .error(R.mipmap.ic_launcher)
                .transform(PicassoUtils.circleTransform())
                .into(mAuthorIcon);

        mAuthorName.setText(answer.getOwner().getName());
        CharSequence body = HtmlCompat.fromHtml(answer.getBody());
        if (maxBodyLength > 0 && body.length() > maxBodyLength) {
            body = TextUtils.concat(body.subSequence(0, maxBodyLength - 3), "...");
        }
        mAnswerBody.setText(body);
        mAnsweredIcon.setVisibility(answer.isAccepted() ? View.VISIBLE : View.INVISIBLE);
        mDivider.setVisibility(isLast ? View.GONE : View.VISIBLE);
    }
}
