package com.roodie.stackoverflowclient.api;

import com.roodie.stackoverflowclient.BuildConfig;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author Roodie
 */

public class ApiKeyInterceptor implements Interceptor {

    private final String mAccessToken;

    public ApiKeyInterceptor() {
        mAccessToken = RepositoryProvider.provideKeyValueStorage().obtainAccessToken();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder()
                .addQueryParameter("key", BuildConfig.KEY_SECRET)
                .addQueryParameter("access_token", mAccessToken)
                .build();
        request = request.newBuilder().url(url).build();
        return chain.proceed(request);
    }
}
