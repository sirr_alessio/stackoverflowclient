package com.roodie.stackoverflowclient.api;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.response.ApiError;
import com.roodie.stackoverflowclient.model.response.ServerError;

import rx.Observable;

/**
 * @author Roodie
 */

public final class ErrorsHandler {

    public ErrorsHandler() {
    }

    @NonNull
    public static <T extends ApiError> Observable.Transformer<T, T> handleErrors() {
        return observable -> observable
                .flatMap(t -> {
                    if (t.isError()) {
                        return Observable.error(new ServerError(t));
                    }
                    return Observable.just(t);
                });
    }
}
