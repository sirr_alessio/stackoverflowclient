package com.roodie.stackoverflowclient.api.service;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.response.ApiError;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * @author Roodie
 */

public interface AuthenticationService {

    @NonNull
    @GET("/apps/{accessTokens}/de-authenticate")
    Observable<ApiError> logout(@NonNull @Path("accessTokens") String token);

}
