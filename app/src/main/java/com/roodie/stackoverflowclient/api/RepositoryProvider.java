package com.roodie.stackoverflowclient.api;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.data.database.LocalRepository;
import com.roodie.stackoverflowclient.data.keyvalue.KeyValueStorage;

/**
 * @author Roodie
 */

public final class RepositoryProvider {

    private static RemoteRepository sRemoteRepository;
    private static LocalRepository sLocalRepository;
    private static KeyValueStorage sKeyValueStorage;

    public RepositoryProvider() {
    }

    @NonNull
    public static RemoteRepository provideRemoteRepository() {
        return sRemoteRepository;
    }

    @NonNull
    public static LocalRepository provideLocalRepository() {
        return sLocalRepository;
    }

    @NonNull
    public static KeyValueStorage provideKeyValueStorage() {
        return sKeyValueStorage;
    }

    public static void setRemoteRepository(@NonNull RemoteRepository remoteRepository) {
        sRemoteRepository = remoteRepository;
    }

    public static void setLocalRepository(@NonNull LocalRepository localRepository) {
        sLocalRepository = localRepository;
    }

    public static void setKeyValueStorage(@NonNull KeyValueStorage keyValueStorage) {
        sKeyValueStorage = keyValueStorage;
    }
}
