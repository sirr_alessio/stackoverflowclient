package com.roodie.stackoverflowclient.api.service;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.response.NotificationResponse;

import retrofit2.http.GET;
import rx.Observable;

/**
 * @author Roodie
 */

public interface NotificationService {

    @NonNull
    @GET("/me/notifications?pagesize=50&filter=!9X8frFZkZ")
    Observable<NotificationResponse> notifications();

}
