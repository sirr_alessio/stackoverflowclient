package com.roodie.stackoverflowclient.api.service;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.response.AnswerResponse;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * @author Roodie
 */

public interface AnswerService {

    @NonNull
    @GET("/users/{ids}/answers?order=desc&sort=creation&pagesize=100&filter=withbody")
    Observable<AnswerResponse> answers(@Path("ids") int userId);

    @NonNull
    @GET("/questions/{ids}/answers?order=desc&sort=creation&filter=withbody")
    Observable<AnswerResponse> questionAnswers(@Path("ids") int questionId);

}
