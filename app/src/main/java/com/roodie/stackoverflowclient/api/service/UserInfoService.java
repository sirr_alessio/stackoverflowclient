package com.roodie.stackoverflowclient.api.service;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.response.BadgeResponse;
import com.roodie.stackoverflowclient.model.response.UserResponse;
import com.roodie.stackoverflowclient.model.response.UserTagResponse;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * @author Roodie
 */

public interface UserInfoService {

    @NonNull
    @GET("/me")
    Observable<UserResponse> getCurrentUser();

    @NonNull
    @GET("/users/{ids}/badges?pagesize=10&order=desc&sort=rank")
    Observable<BadgeResponse> badges(@Path("ids") int userId);

    @NonNull
    @GET("/users/{id}/top-tags?pagesize=10")
    Observable<UserTagResponse> topTags(@Path("id") int userId);

}
