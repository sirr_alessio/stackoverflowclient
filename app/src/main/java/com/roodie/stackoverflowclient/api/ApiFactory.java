package com.roodie.stackoverflowclient.api;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.BuildConfig;
import com.roodie.stackoverflowclient.api.service.AnswerService;
import com.roodie.stackoverflowclient.api.service.AuthenticationService;
import com.roodie.stackoverflowclient.api.service.NotificationService;
import com.roodie.stackoverflowclient.api.service.QuestionService;
import com.roodie.stackoverflowclient.api.service.TagsService;
import com.roodie.stackoverflowclient.api.service.UserInfoService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Roodie
 */

public final class ApiFactory {

    private static OkHttpClient sClient;

    private static UserInfoService sUserInfoService;
    private static QuestionService sQuestionService;
    private static AnswerService sAnswerService;
    private static TagsService sTagsService;
    private static NotificationService sNotificationService;
    private static AuthenticationService sAuthenticationService;

    public ApiFactory() {
    }

    @NonNull
    public static UserInfoService getUserInfoService() {
        UserInfoService service = sUserInfoService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sUserInfoService;
                if (service == null) {
                    service = sUserInfoService = buildRetrofit().create(UserInfoService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    public static QuestionService getQuestionService() {
        QuestionService service = sQuestionService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sQuestionService;
                if (service == null) {
                    service = sQuestionService = buildRetrofit().create(QuestionService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    public static AnswerService getAnswerService() {
        AnswerService service = sAnswerService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sAnswerService;
                if (service == null) {
                    service = sAnswerService = buildRetrofit().create(AnswerService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    public static TagsService getTagsService() {
        TagsService service = sTagsService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sTagsService;
                if (service == null) {
                    service = sTagsService = buildRetrofit().create(TagsService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    public static NotificationService getNotificationService() {
        NotificationService service = sNotificationService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sNotificationService;
                if (service == null) {
                    service = sNotificationService = buildRetrofit().create(NotificationService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    public static AuthenticationService getApplicationService() {
        AuthenticationService service = sAuthenticationService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sAuthenticationService;
                if (service == null) {
                    service = sAuthenticationService = buildRetrofit().create(AuthenticationService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    private static Retrofit buildRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new ApiKeyInterceptor())
                .addInterceptor(new SiteInterceptor())
                .addInterceptor(new HttpLoggingInterceptor())
                .build();
    }
}
