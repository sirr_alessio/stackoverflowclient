package com.roodie.stackoverflowclient.api;

/**
 * @author Roodie
 */

public interface ApiConstants {

    String TAG_ALL = "tag_all";
    String TAG_MY_QUESTIONS = "tag_my_questions";
}
