package com.roodie.stackoverflowclient.api;

import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import com.roodie.sqlite.core.SQLite;
import com.roodie.sqlite.core.Where;
import com.roodie.stackoverflowclient.api.service.AnswerService;
import com.roodie.stackoverflowclient.api.service.AuthenticationService;
import com.roodie.stackoverflowclient.api.service.NotificationService;
import com.roodie.stackoverflowclient.api.service.QuestionService;
import com.roodie.stackoverflowclient.api.service.TagsService;
import com.roodie.stackoverflowclient.api.service.UserInfoService;
import com.roodie.stackoverflowclient.data.database.AnswerTable;
import com.roodie.stackoverflowclient.data.database.NotificationTable;
import com.roodie.stackoverflowclient.data.database.QuestionTable;
import com.roodie.stackoverflowclient.data.database.UserTable;
import com.roodie.stackoverflowclient.model.content.Answer;
import com.roodie.stackoverflowclient.model.content.Badge;
import com.roodie.stackoverflowclient.model.content.Notification;
import com.roodie.stackoverflowclient.model.content.Question;
import com.roodie.stackoverflowclient.model.content.Tag;
import com.roodie.stackoverflowclient.model.content.User;
import com.roodie.stackoverflowclient.model.content.UserTag;
import com.roodie.stackoverflowclient.model.response.AnswerResponse;
import com.roodie.stackoverflowclient.model.response.ApiError;
import com.roodie.stackoverflowclient.model.response.BadgeResponse;
import com.roodie.stackoverflowclient.model.response.NotificationResponse;
import com.roodie.stackoverflowclient.model.response.QuestionResponse;
import com.roodie.stackoverflowclient.model.response.TagResponse;
import com.roodie.stackoverflowclient.model.response.UserResponse;
import com.roodie.stackoverflowclient.model.response.UserTagResponse;
import com.roodie.stackoverflowclient.rx.RxSchedulers;
import com.roodie.stackoverflowclient.utils.TextUtils;

import java.util.List;

import rx.Observable;

/**
 * @author Roodie
 */

public class RemoteRepository {

    private final UserInfoService mUserInfoService;
    private final QuestionService mQuestionService;
    private final AnswerService mAnswerService;
    private final TagsService mTagsService;
    private final NotificationService mNotificationService;
    private final AuthenticationService mAuthenticationService;

    public RemoteRepository(@NonNull UserInfoService userInfoService, @NonNull QuestionService questionService,
                            @NonNull AnswerService answerService, @NonNull TagsService tagsService,
                            @NonNull NotificationService notificationService, @NonNull AuthenticationService authenticationService) {
        mUserInfoService = userInfoService;
        mQuestionService = questionService;
        mAnswerService = answerService;
        mTagsService = tagsService;
        mNotificationService = notificationService;
        mAuthenticationService = authenticationService;
    }

    @NonNull
    public Observable<User> getCurrentUser() {
        return mUserInfoService.getCurrentUser()
                .compose(ErrorsHandler.handleErrors())
                .map(UserResponse::getUsers)
                .map(users -> users.get(0))
                .flatMap(user -> {
                    SQLite.get().insert(UserTable.TABLE, user);
                    RepositoryProvider.provideKeyValueStorage().saveUserId(user.getUserId());
                    //Analytics.setCurrentUser(user);
                    return Observable.just(user);
                })
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<Question>> questions(@NonNull String tag) {
        long toDate = (System.currentTimeMillis() + DateUtils.DAY_IN_MILLIS) / 1000;
        return buildQuestionsObservable(tag, toDate)
                .compose(ErrorsHandler.handleErrors())
                .map(QuestionResponse::getQuestions)
                .flatMap(Observable::from)
                .map(question -> {
                    question.setTag(tag);
                    return question;
                })
                .toList()
                .flatMap(questions -> {
                    SQLite.get().delete(QuestionTable.TABLE, Where.create().equalTo(QuestionTable.TAG, tag));
                    SQLite.get().insert(QuestionTable.TABLE, questions);
                    return Observable.just(questions);
                })
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<Question>> moreQuestions(@NonNull String tag, long toDate) {
        return buildQuestionsObservable(tag, toDate)
                .compose(ErrorsHandler.handleErrors())
                .map(QuestionResponse::getQuestions)
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<Tag>> searchTags(@NonNull String search) {
        return mTagsService.searchTags(search)
                .compose(ErrorsHandler.handleErrors())
                .map(TagResponse::getTags)
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<Badge>> badges(int userId) {
        return mUserInfoService.badges(userId)
                .compose(ErrorsHandler.handleErrors())
                .map(BadgeResponse::getBadges)
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<UserTag>> topTags(int userId) {
        return mUserInfoService.topTags(userId)
                .compose(ErrorsHandler.handleErrors())
                .map(UserTagResponse::getUserTags)
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<Answer>> answers(int userId) {
        return mAnswerService.answers(userId)
                .compose(ErrorsHandler.handleErrors())
                .map(AnswerResponse::getAnswers)
                .flatMap(answers -> {
                    SQLite.get().delete(AnswerTable.TABLE);
                    SQLite.get().insert(AnswerTable.TABLE, answers);
                    return Observable.just(answers);
                })
                .onErrorResumeNext(throwable -> {
                    List<Answer> answers = SQLite.get().query(AnswerTable.TABLE);
                    if (answers.isEmpty()) {
                        return Observable.error(throwable);
                    }
                    return Observable.just(answers);
                })
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<Question> questionWithBody(int questionId) {
        return mQuestionService.questionWithBody(questionId)
                .compose(ErrorsHandler.handleErrors())
                .map(QuestionResponse::getQuestions)
                .map(questions -> questions.get(0))
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<Answer>> questionAnswers(int questionId) {
        return mAnswerService.questionAnswers(questionId)
                .compose(ErrorsHandler.handleErrors())
                .map(AnswerResponse::getAnswers)
                .compose(RxSchedulers.async());
    }

    @NonNull
    public Observable<List<Notification>> notifications() {
        return mNotificationService.notifications()
                .compose(ErrorsHandler.handleErrors())
                .map(NotificationResponse::getNotifications)
                .flatMap(Observable::from)
                .filter(notification -> !TextUtils.isEmpty(notification.getBody()))
                .toList()
                .flatMap(notifications -> {
                    SQLite.get().delete(NotificationTable.TABLE);
                    SQLite.get().insert(NotificationTable.TABLE, notifications);
                    return Observable.just(notifications);
                });
    }

    @NonNull
    public Observable<ApiError> logout(@NonNull String token) {
        return mAuthenticationService.logout(token)
                .compose(ErrorsHandler.handleErrors())
                .compose(RxSchedulers.async());
    }

    @NonNull
    private Observable<QuestionResponse> buildQuestionsObservable(@NonNull String tag, long toDate) {
        Observable<QuestionResponse> questionsObservable;
        if (TextUtils.equals(ApiConstants.TAG_ALL, tag)) {
            questionsObservable = mQuestionService.questions(toDate);
        } else if (TextUtils.equals(ApiConstants.TAG_MY_QUESTIONS, tag)) {
            questionsObservable = mQuestionService.myQuestions(toDate);
        } else {
            questionsObservable = mQuestionService.questions(tag, toDate);
        }
        return questionsObservable;
    }
}