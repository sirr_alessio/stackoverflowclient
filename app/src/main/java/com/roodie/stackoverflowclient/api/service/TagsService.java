package com.roodie.stackoverflowclient.api.service;

import android.support.annotation.NonNull;

import com.roodie.stackoverflowclient.model.response.TagResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * @author Roodie
 */

public interface TagsService {

    @GET("tags?order=desc&sort=popular&pagesize=50")
    Observable<TagResponse> searchTags(@NonNull @Query("inname") String search);

}
