package com.roodie.stackoverflowclient.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.Button;

import com.roodie.stackoverflowclient.app.Env;
import com.roodie.stackoverflowclient.app.analytics.Analytics;
import com.roodie.stackoverflowclient.app.analytics.EventKeys;
import com.roodie.stackoverflowclient.app.analytics.EventTags;
import com.roodie.stackoverflowclient.model.content.Badge;


/**
 * @author Roodie
 */
public class BadgeButton extends Button {

    public BadgeButton(Context context) {
        super(context);
    }

    public BadgeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BadgeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setBadge(@NonNull Badge badge) {
        setText(badge.getName());
        setOnClickListener(view -> {
            Analytics.buildEvent()
                    .putString(EventKeys.PROFILE_BADGE, badge.getName())
                    .log(EventTags.PROFILE_BADGE_CLICKED);
            Env.browseUrl(getContext(), badge.getLink());
        });
    }

}
