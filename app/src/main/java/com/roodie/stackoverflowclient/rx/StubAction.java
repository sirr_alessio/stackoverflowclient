package com.roodie.stackoverflowclient.rx;

import rx.functions.Action1;

/**
 * @author Roodie
 */
public class StubAction<T> implements Action1<T> {

    @Override
    public void call(T t) {
        // Do nothing
    }
}
