package com.roodie.stackoverflowclient.model.content;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Roodie
 */

public class User implements Serializable{

    private static final long serialVersionUID = User.class.getName().hashCode();

    public static final String TABLE = "user";

    public static final String USER_ID = "user_id";
    public static final String AGE = "age";
    public static final String NAME = "name";
    public static final String REPUTATION = "reputation";
    public static final String LINK = "link";
    public static final String PROFILE_IMAGE = "profile_image";

    @SerializedName("user_id")
    private int mUserId;

    @SerializedName("age")
    private int mAge;

    @SerializedName("display_name")
    private String mName;

    @SerializedName("reputation")
    private int mReputation;

    @SerializedName("link")
    private String mLink;

    @SerializedName("profile_image")
    private String mProfileImage;

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public int getAge() {
        return mAge;
    }

    public void setAge(int age) {
        mAge = age;
    }

    @NonNull
    public String getName() {
        return mName;
    }

    public void setName(@NonNull String name) {
        mName = name;
    }

    public int getReputation() {
        return mReputation;
    }

    public void setReputation(int reputation) {
        mReputation = reputation;
    }

    @NonNull
    public String getLink() {
        return mLink;
    }

    public void setLink(@NonNull String link) {
        mLink = link;
    }

    @NonNull
    public String getProfileImage() {
        return mProfileImage;
    }

    public void setProfileImage(@NonNull String profileImage) {
        mProfileImage = profileImage;
    }


}
