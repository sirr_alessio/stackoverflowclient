package com.roodie.stackoverflowclient.model.content;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.roodie.stackoverflowclient.app.GsonHolder;


/**
 * @author Roodie
 */

public class Answer {

    public static final String TABLE_NAME = "answer";

    public static final String OWNER = "owner";
    public static final String BODY = "body";
    public static final String IS_ACCEPTED = "is_accepted";
    public static final String QUESTION_ID = "question_id";
    public static final String ANSWER_ID = "answer_id";


    @SerializedName("owner")
    private User mOwner;

    @SerializedName("body")
    private String mBody;

    @SerializedName("is_accepted")
    private boolean mIsAccepted;

    @SerializedName("question_id")
    private int mQuestionId;

    @SerializedName("answer_id")
    private int mAnswerId;

    @NonNull
    public User getOwner() {
        return mOwner;
    }

    public void setOwner(@NonNull User owner) {
        mOwner = owner;
    }

    @NonNull
    public String getBody() {
        return mBody;
    }

    public void setBody(@NonNull String body) {
        mBody = body;
    }

    public boolean isAccepted() {
        return mIsAccepted;
    }

    public void setAccepted(boolean accepted) {
        mIsAccepted = accepted;
    }

    public int getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(int questionId) {
        mQuestionId = questionId;
    }

    public int getAnswerId() {
        return mAnswerId;
    }

    public void setAnswerId(int answerId) {
        mAnswerId = answerId;
    }



}
