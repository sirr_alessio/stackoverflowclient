package com.roodie.stackoverflowclient.model.response;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.roodie.stackoverflowclient.model.content.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roodie
 */

public class NotificationResponse extends ApiError {

    @SerializedName("items")
    private List<Notification> mNotifications;

    @NonNull
    public List<Notification> getNotifications() {
        if (mNotifications == null) {
            mNotifications = new ArrayList<>();
        }
        return mNotifications;
    }

    public void setNotifications(@NonNull List<Notification> notifications) {
        mNotifications = notifications;
    }
}
