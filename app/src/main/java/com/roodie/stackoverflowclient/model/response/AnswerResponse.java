package com.roodie.stackoverflowclient.model.response;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.roodie.stackoverflowclient.model.content.Answer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roodie
 */

public class AnswerResponse extends ApiError {

    @SerializedName("items")
    private List<Answer> mAnswers;

    @NonNull
    public List<Answer> getAnswers() {
        if (mAnswers == null) {
            mAnswers = new ArrayList<>();
        }
        return mAnswers;
    }

    public void setAnswers(@NonNull List<Answer> answers) {
        mAnswers = answers;
    }
}
