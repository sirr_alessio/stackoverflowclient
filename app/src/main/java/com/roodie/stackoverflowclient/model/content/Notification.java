package com.roodie.stackoverflowclient.model.content;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import rx.functions.Func1;

/**
 * @author Roodie
 */

public class Notification {

    public static final String TABLE = "notification";

    public static final String IS_UNREAD = "is_unread";
    public static final String CREATION_DATE = "creation_date";
    public static final String BODY = "body";

    @SerializedName("is_unread")
    private boolean mIsUnread;

    @SerializedName("creation_date")
    private long mCreationDate;

    @SerializedName("body")
    private String mBody;

    public boolean isUnread() {
        return mIsUnread;
    }

    public void setUnread(boolean unread) {
        mIsUnread = unread;
    }

    public long getCreationDate() {
        return mCreationDate;
    }

    public void setCreationDate(long creationDate) {
        mCreationDate = creationDate;
    }

    @NonNull
    public String getBody() {
        return mBody;
    }

    public void setBody(@NonNull String body) {
        mBody = body;
    }


}

