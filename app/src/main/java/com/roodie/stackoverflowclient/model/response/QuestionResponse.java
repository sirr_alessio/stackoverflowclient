package com.roodie.stackoverflowclient.model.response;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.roodie.stackoverflowclient.model.content.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Roodie
 */

public class QuestionResponse extends ApiError {

    @SerializedName("items")
    private List<Question> mQuestions;

    @NonNull
    public List<Question> getQuestions() {
        if (mQuestions == null) {
            mQuestions = new ArrayList<>();
        }
        return mQuestions;
    }

    public void setQuestions(@NonNull List<Question> questions) {
        mQuestions = questions;
    }
}
