package com.roodie.stackoverflowclient.model.content;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.roodie.stackoverflowclient.api.ApiConstants;
import com.roodie.stackoverflowclient.utils.TextUtils;

import java.io.Serializable;

/**
 * @author Roodie
 */

public class Question implements Serializable {

    private static final long serialVersionUID = Question.class.getName().hashCode();

    public static final String TABLE = "question";

    public static final String QUESTION_ID = "question_id";
    public static final String TITLE = "title";
    public static final String LINK = "link";
    public static final String OWNER = "owner";
    public static final String IS_ANSWERED = "is_answered";
    public static final String VIEW_COUNT = "view_count";
    public static final String ANSWER_COUNT = "answer_count";
    public static final String CREATION_DATE = "creation_date";
    public static final String TAG = "tag";

    @SerializedName("question_id")
    private int mQuestionId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("body")
    private String mBody;

    @SerializedName("link")
    private String mLink;

    @SerializedName("owner")
    private User mOwner;

    @SerializedName("is_answered")
    private boolean mIsAnswered;

    @SerializedName("view_count")
    private int mViewCount;

    @SerializedName("answer_count")
    private int mAnswerCount;

    @SerializedName("creation_date")
    private long mCreationDate;

    private String mTag;

    public int getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(int questionId) {
        mQuestionId = questionId;
    }

    @NonNull
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(@NonNull String title) {
        mTitle = title;
    }

    @NonNull
    public String getBody() {
        return mBody;
    }

    public void setBody(@NonNull String body) {
        mBody = body;
    }

    @NonNull
    public String getLink() {
        return mLink;
    }

    public void setLink(@NonNull String link) {
        mLink = link;
    }

    @NonNull
    public User getOwner() {
        return mOwner;
    }

    public void setOwner(@NonNull User owner) {
        mOwner = owner;
    }

    public boolean isAnswered() {
        return mIsAnswered;
    }

    public void setAnswered(boolean answered) {
        mIsAnswered = answered;
    }

    public int getViewCount() {
        return mViewCount;
    }

    public void setViewCount(int viewCount) {
        mViewCount = viewCount;
    }

    public int getAnswerCount() {
        return mAnswerCount;
    }

    public void setAnswerCount(int answerCount) {
        mAnswerCount = answerCount;
    }

    public long getCreationDate() {
        return mCreationDate;
    }

    public void setCreationDate(long creationDate) {
        mCreationDate = creationDate;
    }

    @NonNull
    public String getTag() {
        if (TextUtils.isEmpty(mTag)) {
            mTag = ApiConstants.TAG_ALL;
        }
        return mTag;
    }

    public void setTag(@NonNull String tag) {
        mTag = tag;
    }



}
