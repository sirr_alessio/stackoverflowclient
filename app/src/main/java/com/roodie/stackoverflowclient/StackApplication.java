package com.roodie.stackoverflowclient;

import android.app.Application;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.google.firebase.iid.FirebaseInstanceId;
import com.roodie.sqlite.core.SQLite;
import com.roodie.stackoverflowclient.api.RepositoryProvider;
import com.roodie.stackoverflowclient.app.analytics.Analytics;
import com.roodie.stackoverflowclient.app.analytics.EventTags;
import com.roodie.stackoverflowclient.data.keyvalue.HawkStorage;
import com.roodie.stackoverflowclient.utils.PicassoUtils;
import com.roodie.stackoverflowclient.utils.TextUtils;

import io.fabric.sdk.android.Fabric;
import rx.schedulers.Schedulers;

/**
 * Created by Roodie on 09.10.2016.
 */

public class StackApplication extends Application {

    private static StackApplication sInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Analytics.init(this);
        Analytics.buildEvent().log(EventTags.APP_STARTED);

        SQLite.initialize(this);

        Stetho.initializeWithDefaults(this);

        RepositoryProvider.setKeyValueStorage(new HawkStorage(this));

        PicassoUtils.setup(this);

        String token = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(token)) {
            Analytics.setFcmRegistrationKey(token);
        }

        sInstance = this;
    }


    @NonNull
    public static StackApplication get() {
        return sInstance;
    }

}
