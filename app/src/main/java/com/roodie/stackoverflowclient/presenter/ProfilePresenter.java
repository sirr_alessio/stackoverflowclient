package com.roodie.stackoverflowclient.presenter;

import android.app.LoaderManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import com.roodie.stackoverflowclient.R;
import com.roodie.stackoverflowclient.api.RemoteRepository;
import com.roodie.stackoverflowclient.api.RepositoryProvider;
import com.roodie.stackoverflowclient.model.content.Badge;
import com.roodie.stackoverflowclient.model.content.User;
import com.roodie.stackoverflowclient.model.content.UserTag;
import com.roodie.stackoverflowclient.rx.RxDecor;
import com.roodie.stackoverflowclient.rx.rxloader.RxLoader;
import com.roodie.stackoverflowclient.view.ErrorView;
import com.roodie.stackoverflowclient.view.LoadingView;
import com.roodie.stackoverflowclient.view.ProfileView;

import java.util.List;

import rx.Observable;

/**
 * @author Roodie
 */
public class ProfilePresenter {

    private static final String ERROR_SHOWN_KEY = "error_shown";

    private static final String LINK_FORMAT = "<a href=\"%1$s\">%2$s</a>";

    private final Context mContext;
    private final LoaderManager mLm;

    private final ProfileView mView;
    private final LoadingView mLoadingView;
    private final ErrorView mErrorView;

    private final User mUser;

    private boolean mIsErrorShown = false;

    public ProfilePresenter(@NonNull Context context, @NonNull LoaderManager lm, @NonNull ProfileView view,
                            @NonNull LoadingView loadingView, @NonNull ErrorView errorView, @NonNull User user) {
        mContext = context;
        mLm = lm;
        mView = view;
        mLoadingView = loadingView;
        mErrorView = errorView;
        mUser = user;
    }

    public void init(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mIsErrorShown = savedInstanceState.getBoolean(ERROR_SHOWN_KEY);
        }

        RemoteRepository repository = RepositoryProvider.provideRemoteRepository();
        int userId = mUser.getUserId();
        Observable<Pair<List<Badge>, List<UserTag>>> observable =
                Observable.zip(repository.badges(userId), repository.topTags(userId), Pair::create)
                        .compose(RxDecor.loading(mLoadingView));

        RxLoader.create(mContext, mLm, R.id.user_full_info_loader_id, observable)
                .init(pair -> {
                    List<Badge> badges = pair.first;
                    List<UserTag> userTags = pair.second;

                    showUserInfo();
                    if (badges != null && !badges.isEmpty()) {
                        mView.showBadges(badges);
                    }
                    if (userTags != null && !userTags.isEmpty()) {
                        mView.showTopTags(userTags);
                    }
                }, throwable -> {
                    showUserInfo();
                    if (!mIsErrorShown) {
                        mIsErrorShown = true;
                        RxDecor.error(mErrorView).call(throwable);
                    }
                });
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(ERROR_SHOWN_KEY, mIsErrorShown);
    }

    private void showUserInfo() {
        mView.showUserName(mUser.getName());
        String imageLink = mUser.getProfileImage().replace("s=128", "s=800").replace("sz=128", "sz=800");
        mView.showUserImage(imageLink);
        mView.showReputation(mContext.getString(R.string.reputation, mUser.getReputation()));
        mView.showProfileLink(String.format(LINK_FORMAT, mUser.getLink(), mContext.getString(R.string.profile_text)));
    }
}
